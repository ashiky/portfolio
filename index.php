<?php include("header.php") ?>
   <main>
        <h2>Mes Projets</h2>
        <div class="container">
            <button class="buttonSlide slide-arrow-prev"><</button>
                    <ul class="slides-container" id="slides-container">
                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=1" class="lien-projet"><span></span></a>
                                <img src="images/simplonline.png" alt="screen de mon preojet simplonline">
                                <h3>Simplonline</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>

                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=2" class="lien-projet"><span></span></a>
                                <img src="images/ecomove.png" alt="screen de mon projet ecomove">
                                <h3>Ecomove</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>

                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=3" class="lien-projet"><span></span></a>
                                <img src="images/barber.png" alt="screen de mon projet barber">
                                <h3>Barber</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>
                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=4" class="lien-projet"><span></span></a>
                                <img src="images/appetit.png" alt="screen de mon preojet appetit">
                                <h3>appetit</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>

                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=5" class="lien-projet"><span></span></a>
                                <img src="images/cv.png" alt="screen de mon projet cv">
                                <h3>CV</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>

                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=6" class="lien-projet"><span></span></a>
                                <img src="images/calc.png" alt="screen de mon projet calculatrice">
                                <h3>calculatrice</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>
                        <li class="slide">
                            <div>
                            <a href="http://127.0.0.1:8000/project.php?projet=7" class="lien-projet"><span></span></a>
                                <img src="images/blog.png" alt="screen de mon preojet blog">
                                <h3>Simplonline</h3>
                                <p>Petite description du projet</p>
                            </div>
                        </li>
                    </ul>
                    <button class="buttonSlide slide-arrow-next">></button>
        </div>
        <h2>CV</h2>
        <div class="containerCV" id="containerCV">
            <div>
                <div class="descCV">
                    <div>
                        <h3>Nom :</h3>
                        <h3>Berot</h3>
                    </div>
                    <div>
                        <h3>Prenom :</h3>
                        <h3>Anthony</h3>
                    </div>
                    <div>
                        <h3>Age :</h3>
                        <h3>Berot</h3>
                    </div>
                </div>
                <div class="descCV">
                    <div>
                        <h3>Plus haut diplome :</h3>
                        <h3>CAP</h3>
                    </div>
                    <div>
                        <h3>Plus recent diplome :</h3>
                        <h3>CAP</h3>
                    </div>
                    <div>
                        <h3>Activité actuel :</h3>
                        <h3>Dev Web</h3>
                    </div>
                </div>
                <div class="descCV">
                    <div>
                        <h3>Ville :</h3>
                        <h3>Toulouse</h3>
                    </div>
                    <div>
                        <h3>Entreprise actuel :</h3>
                        <h3>Simplon</h3>
                    </div>
                    <div>
                        <h3>Nom :</h3>
                        <h3>Berot</h3>
                    </div>
                </div>
            </div>
            <h3>Mon parcours</h3>
            <p>
                Après avoir travaillé pendant 5 ans dans la patisserie, je me suis reconverti vers le développement.
                 J'ai commencé par essayé d'apprendre par moi meme mais je n'ai pas réussi.
                Je me suis alors tourné vers une formation Simplon, l'AFP (Apple Fondation Programme).
                Durant cette formation j'ai pu découvrir les bases du développemnt d'application avec swift.
                Jai poursuivi dans cette voix avec l'AFE (Apple Formation Extendeed).
                Je suis actuelement en formatation de développeur Web/Mobile toujours auprès de Simplon.
            </p>
            <button id="lire-suite">Lire la suite...</button>
        </div>
    </main>
    <?php include("footer.php") ?>
